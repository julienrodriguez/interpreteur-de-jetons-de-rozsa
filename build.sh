#!/usr/bin/env bash

rm -rf public
mkdir -p public

# Copy static assets
rsync -azh static/ public/

# Bundle JavaScript scripts
mkdir -p public/js
npx rollup js/main.js --file public/js/main.js --format iife
npx rollup js/evaluator/worker.js --file public/js/worker.js --format iife
