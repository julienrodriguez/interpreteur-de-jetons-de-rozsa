import Interface from "./Interface.js";
import Container from "./Container.js";

const container = window.container = new Container();
container.load().then(() => {
	window.interface = new Interface(container);
});
