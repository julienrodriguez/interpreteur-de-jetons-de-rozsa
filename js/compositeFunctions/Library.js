import AbstractComposite from "./AbstractComposite.js";

class Library extends AbstractComposite
{
	constructor(child, name, sequence, description, args = [], code = null)
	{
		super(child, name, sequence, description, args);
		this._code = code;
	}
	
	get token()
	{
		let frame = this._tokenFrame;
		
		if (Library.cheat && this._code !== null)
		{
			frame.classList.add("token--library-cheat");
		}
		else
		{
			frame.classList.add("token--library");
		}
			
		frame.firstChild.textContent = this.name;

		return frame;
	}
	
	evaluate(args)
	{
		if (Library.cheat && this._code !== null)
		{
			return [eval(this._code), 0];
		}
		else
		{
			return super.evaluate(args);
		}
	}
	
	reduceAfter(expr)
	{
		if (Library.cheat && this._code !== null)
		{
			return expr.args;
		}
		else
		{
			return super.reduceAfter(expr);
		}
	}
	
	reduce(expr)
	{
		if (Library.cheat && this._code !== null)
		{
			let args = expr.args.map(arg => arg.value);
			expr.value = eval(this._code);
			expr.args = [];
		}
		else
		{
			super.reduce(expr);
		}
	}
	
	clone()
	{
		return new this.constructor(this.children[0].clone(), this._name, this._sequence, this.description, this.args, this._code);
	}
}

Library.cheat = false;

export default Library;
