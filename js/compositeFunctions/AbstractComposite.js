import * as rfunctions from "../rfunctions/index.js";

class AbstractComposite extends rfunctions.Abstract
{
	constructor(child, name, sequence, description, args = [])
	{
		super([child]);

		this._name = name;
		this._sequence = sequence;
		this.description = description;
		this.args = args;

		this.registeredResults = {};
	}

	getSequence(complete = false)
	{
		if (complete)
			return this.sequence;
		else
			return this._sequence;
	}

	// TODO: reimplement memoization inside reduce()
	//
	// apply(args)
	// {
	// 	var stringifiedArgs = JSON.stringify(args);
	// 	if (this.registeredResults.hasOwnProperty(stringifiedArgs))
	// 		return this.registeredResults[stringifiedArgs];

	// 	this.registeredResults[stringifiedArgs] = this.children[0].apply(args);

	// 	return this.registeredResults[stringifiedArgs];
	// }

	reduce(expr)
	{
		expr.value = this.children[0];
	}

	get name()
	{
		return this._name;
	}

	get arity()
	{
		return this.children[0].arity;
	}

	validate()
	{
		super.validate();

		if (this.children.length !== 1)
		{
			throw new SyntaxError("A composite token must contain exactly one "
				+ "child token");
		}
	}

	canAddChild()
	{
		return this.children.length < 1;
	}

	get sequence()
	{
		return this.children[0].sequence;
	}

	isFinal()
	{
		return this.valid;
	}

	clone()
	{
		return new this.constructor(this.children[0].clone(), this._name, this._sequence, this.description, this.args);
	}
}

export default AbstractComposite;
