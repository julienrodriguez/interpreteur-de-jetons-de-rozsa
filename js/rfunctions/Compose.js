import Abstract from "./Abstract.js";
import Expression from "../evaluator/Expression.js";

class Compose extends Abstract
{
	reduce(expr)
	{
		// Apply the first child function to the result of applying the other
		// children to the arguments. The same arguments are re-used for all
		// children so that the same values aren’t computed twice
		expr.value = this.children[0];
		expr.args = this.children.slice(1).map(
			child => new Expression(child, expr.args));
	}

	get name()
	{
		return "C";
	}

	get arity()
	{
		if (this.children.length <= 1)
		{
			return 0;
		}
		else
		{
			return this.children[1].arity;
		}
	}

	validate()
	{
		super.validate();

		if (this.children.length === 0)
		{
			throw new SyntaxError("The composition constructor expects at "
				+ "least one child token");
		}

		if (this.children[0].arity !== this.children.length - 1)
		{
			throw new SyntaxError(`The number of tokens following the first \
one (${this.children.length - 1} given) must exactly match the first token’s \
arity (${this.children[0].arity} expected)`);
		}

		for (let i = 2; i < this.children.length; ++i)
		{
			const child = this.children[i];

			if (child.arity !== this.children[1].arity)
			{
				throw new SyntaxError(`All tokens following the first one must \
have the same arity, token n°${i + 1} has arity ${child.arity} while token \
n°2 has arity ${this.children[1].arity}`);
			}
		}
	}

	canAddChild()
	{
		return this.children.length === 0
			|| this.children.length < this.children[0].arity + 1;
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.classList.add("token--constructor");
		frame.firstChild.classList.add("token__text--composition");

		return frame;
	}
}

export default Compose;
