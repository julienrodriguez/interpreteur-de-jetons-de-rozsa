import Abstract from "./Abstract.js";

class Identity extends Abstract
{
	reduce(expr)
	{
		// Replace `I(x)` with `x` where `x` is an expression
		expr.value = expr.args[0].value;
		expr.args = expr.args[0].args;
	}

	get name()
	{
		return "I";
	}

	get arity()
	{
		return 1;
	}

	validate()
	{
		super.validate();

		if (this.children.length > 0)
		{
			throw SyntaxError("The identity token expects no child tokens");
		}
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = this.name;
		frame.classList.add("token--basic");

		return frame;
	}

	isFinal()
	{
		return true;
	}
}

export default Identity;
