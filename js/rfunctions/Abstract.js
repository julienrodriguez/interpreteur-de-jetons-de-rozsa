class Abstract
{
	constructor(children = [])
	{
		if (this.constructor === Abstract)
		{
			throw new TypeError("Abstract class \"AbstractRFunction\" cannot be instantiated directly");
		}

		this.children = [];
		this.description = "Base function.";

		for (let child of children)
		{
			this.addChild(child);
		}
	}

	validate()
	{
		for (let child of this.children)
		{
			child.validate();
		}
	}

	canAddChild()
	{
		return false;
	}

	addChild(child)
	{
		if (!this.canAddChild())
		{
			throw new Error("Cannot add a new child.");
		}

		this.children.push(child);
	}

	/**
	 * Select a subset of arguments of an expression to which this function
	 * has been applied that must be reduced before that expression can be
	 * reduced.
	 *
	 * @param {Expression} expr Expression to which this function is applied.
	 * @return {Array.<Expression>} Subset of arguments that must be reduced
	 * first.
	 */
	reduceAfter()
	{
		// By default, we do not reduce any argument (lazy evaluation).
		return [];
	}

	/**
	 * Reduce an expression to which this function has been applied.
	 *
	 * @param {Expression} expr Expression which this function is applied.
	 */
	reduce()
	{
		throw new Error("This method has not been implemented!");
	}

	/**
	 * Get the number of arguments that this function expects.
	 *
	 * @return {number} Number of arguments expected.
	 */
	get arity()
	{
		throw new Error("This method has not been implemented!");
	}

	/**
	 * Get the sequence of tokens that make up this expression.
	 *
	 * @return {string} Sequence of tokens.
	 */
	get sequence()
	{
		return this.children.reduce(
			(sequence, child) => sequence.concat(child.sequence),
			[this.name]
		);
	}

	get name()
	{
		throw new Error("This method has not been implemented!");
	}

	get code()
	{
		let code = 0;

		this.sequence.forEach(e =>
		{
			code *= 8;
			code += Abstract.charToInterger[e];
		});

		return code;
	}

	get _tokenFrame()
	{
		let tokenFrame = document.createElement("span");

		let tokenText = document.createElement("span");
		tokenText.contentEditable = false;
		tokenFrame.contentEditable = false;
		tokenFrame.appendChild(tokenText);

		tokenFrame.className = "token";
		tokenText.className = "token__text";

		return tokenFrame;
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = this.name;

		return frame;
	}

	isFinal()
	{
		return false;
	}

	clone()
	{
		return new this.constructor(this.children.map(child => child.clone()));
	}

	getSequence()
	{
		return this.sequence;
	}
}

Abstract.charToInterger
= {
		"0" : 0,
		"I" : 1,
		"S" : 2,
		"<" : 3,
		">" : 4,
		"C" : 5,
		"R" : 6,
		"µ" : 7,
	};

export default Abstract;
