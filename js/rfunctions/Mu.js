import Abstract from "./Abstract.js";
import Expression from "../evaluator/Expression.js";

class Mu extends Abstract
{
	reduceAfter(expr)
	{
		if (expr.args.length === this.arity + 2)
		{
			// Recursive evaluation: we need to reduce the penultimate argument,
			// which is the expression `f(n, ...args)`, before we can know
			// whether we found a suitable value
			return [expr.args[this.arity]];
		}
		else
		{
			// Initial evaluation: we were called with just enough arguments
			// to pass to the children. These need not be reduced
			return [];
		}
	}

	reduce(expr)
	{
		const args = expr.args;
		const childArgs = args.slice(0, this.arity);

		// Upon recursive evaluations, we add two extra arguments to remember
		// the result expression and the n-value that are being tested
		const [
			result = new Expression(-1),
			n = new Expression(-1)
		] = args.slice(this.arity);

		if (result.value === 0)
		{
			// We found a suitable n-value for which the child function
			// yielded 0, we can stop recursing
			expr.value = n.value;
			expr.args = [];
		}
		else
		{
			// Otherwise, continue unbounded recursion, incrementing n
			expr.args = [
				...childArgs,
				new Expression(this.children[0], [
					new Expression(n.value + 1),
					...childArgs
				]),
				new Expression(n.value + 1),
			];
		}
	}

	get name()
	{
		return "M";
	}

	get arity()
	{
		return this.children[0].arity - 1;
	}

	validate()
	{
		super.validate();

		if (this.children.length !== 1)
		{
			throw new SyntaxError(`The μ constructor expects exactly one child \
token (${this.children.length} given)`);
		}

		if (this.children[0].arity === 0)
		{
			throw new SyntaxError("The child token of the μ constructor must "
				+ "accept at least one argument");
		}
	}

	canAddChild()
	{
		return this.children.length < 1;
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = "μ";
		frame.classList.add("token--constructor");

		return frame;
	}
}

export default Mu;
