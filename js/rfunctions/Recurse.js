import Abstract from "./Abstract.js";
import Expression from "../evaluator/Expression.js";

class Recurse extends Abstract
{
	reduceAfter(expr)
	{
		// We need to know the recursion step before starting to recurse,
		// therefore we ask for reduction of the first argument
		return expr.args.slice(0, 1);
	}

	reduce(expr)
	{
		const [n, ...args] = expr.args;

		if (n.value === 0)
		{
			expr.value = this.children[0];
			expr.args = args;
		}
		else
		{
			expr.value = this.children[1];
			expr.args = [
				new Expression(n.value - 1),
				new Expression(this, [
					new Expression(n.value - 1),
					...args
				]),
				...args
			];
		}
	}

	get name()
	{
		return "R";
	}

	get arity()
	{
		return this.children[0].arity + 1;
	}

	validate()
	{
		super.validate();

		if (this.children.length !== 2)
		{
			throw new SyntaxError(`The recurse constructor expects exactly two \
child tokens (${this.children.length} given)`);
		}

		if (this.children[1].arity !== this.children[0].arity + 2)
		{
			throw new SyntaxError(`The second child of the recurse constructor \
(of arity ${this.children[1].arity}) must accept exactly two more arguments \
than the first child (of arity ${this.children[0].arity})`);
		}
	}

	canAddChild()
	{
		return this.children.length < 2;
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = this.name;
		frame.classList.add("token--constructor");

		return frame;
	}
}

export default Recurse;
