import Abstract from "./Abstract.js";

class Successor extends Abstract
{
	reduceAfter(expr)
	{
		// To compute the argument’s successor, it first needs to
		// be fully reduced
		return expr.args;
	}

	reduce(expr)
	{
		// The argument has been fully reduced, we thus simply
		// take its successor value
		expr.value = expr.args[0].value + 1;
		expr.args = [];
	}

	get name()
	{
		return "S";
	}

	get arity()
	{
		return 1;
	}

	validate()
	{
		super.validate();

		if (this.children.length > 0)
		{
			throw SyntaxError("The successor token expects no child tokens");
		}
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = this.name;
		frame.classList.add("token--basic");

		return frame;
	}

	isFinal()
	{
		return true;
	}
}

export default Successor;
