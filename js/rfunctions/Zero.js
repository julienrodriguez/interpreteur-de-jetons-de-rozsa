import Abstract from "./Abstract.js";

class Zero extends Abstract
{
	reduce(expr)
	{
		// Replace with the value 0
		expr.value = 0;
		expr.args = [];
	}

	get name()
	{
		return "0";
	}

	get arity()
	{
		return 0;
	}

	validate()
	{
		super.validate();

		if (this.children.length > 0)
		{
			throw SyntaxError("The zero token expects no child tokens");
		}
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.firstChild.textContent = this.name;
		frame.classList.add("token--basic");

		return frame;
	}

	isFinal()
	{
		return true;
	}
}

export default Zero;
