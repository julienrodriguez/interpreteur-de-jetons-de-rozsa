import Abstract from "./Abstract.js";

class Right extends Abstract
{
	reduce(expr)
	{
		// Apply the child function to all arguments except the last one
		expr.value = this.children[0];
		expr.args = expr.args.slice(0, -1);
	}

	get name()
	{
		return ">";
	}

	get arity()
	{
		return this.children[0].arity + 1;
	}

	validate()
	{
		super.validate();

		if (this.children.length !== 1)
		{
			throw new SyntaxError(`The right constructor expects exactly one \
child token (${this.children.length} given)`);
		}
	}

	canAddChild()
	{
		return this.children.length < 1;
	}

	get token()
	{
		let frame = this._tokenFrame;
		frame.classList.add("token--arity");
		frame.firstChild.classList.add("token__text--right");

		return frame;
	}
}

export default Right;
