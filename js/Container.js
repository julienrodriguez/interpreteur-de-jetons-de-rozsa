import * as rfunctions from "./rfunctions/index.js";
import * as compositeFunctions from "./compositeFunctions/index.js";

const toChar = [
	"0",
	"I",
	"S",
	"<",
	">",
	"C",
	"R",
	"µ",
];

class Container
{
	constructor()
	{
		// Map available basic R-function names to their class
		this._baseRFunctions = [
			new rfunctions.Zero(),
			new rfunctions.Identity(),
			new rfunctions.Successor(),
			new rfunctions.Left(),
			new rfunctions.Right(),
			new rfunctions.Compose(),
			new rfunctions.Recurse(),
			new rfunctions.Mu(),
		].reduce(
			(map, token) => ({
				...map,
				[token.name]: token
			}),
			{}
		);

		this.instances = {};
	}

	load()
	{
		return this
			.loadLibrary()
			.then(() => this.loadUserFunctions());
	}

	loadLibrary()
	{
		return fetch("json/library.json")
			.then(response => response.json())
			.then(library => {
				this._library = {
					categories: {},
					functions: {},
				};

				for (let [categoryKey, category] of Object.entries(library))
				{
					const name = category.name;
					delete category['name'];

					this._library.categories[categoryKey] = {
						name,
						functions: Object.keys(category)
					};

					for (let [funcKey, func] of Object.entries(category))
					{
						this._library.functions[funcKey] = func;
					}
				}
			});
	}

	loadUserFunctions()
	{
		if (localStorage.getItem("userFunctions") !== null && JSON.parse(localStorage.getItem("userFunctions")) !== null)
		{
			this._userFunctions = JSON.parse(localStorage.getItem("userFunctions"));
		}
		else
		{
			this._userFunctions = {};
		}

		// User library loading is synchronous due to the local storage API,
		// but return a promise anyway to keep the interface consistent with
		// Container#loadLibrary()
		return Promise.resolve();
	}

	addUserFunction(userFunction)
	{
		if (!(userFunction instanceof compositeFunctions.User))
			throw new Error("You can only store user functions !");

		if (userFunction.name !== null && userFunction.name.length !== 0 && !this.exist(userFunction.name))
		{
			this._userFunctions[userFunction.name] = {"description" : userFunction.description, "tokens" : userFunction.getSequence()};
			localStorage.setItem("userFunctions", JSON.stringify(this._userFunctions));
			return true;
		}

		throw new Error("You can't save this function : invalid name or the function already exists.");
	}

	exist(rFunction)
	{
		return this.baseRFunctions.hasOwnProperty(rFunction)
            || this.libraryFunctions.hasOwnProperty(rFunction)
            || this.userFunctions.hasOwnProperty(rFunction);
	}

	get baseRFunctions()
	{
		return this._baseRFunctions;
	}

	get libraryFunctions()
	{
		return this._library.functions;
	}

	get userFunctions()
	{
		return this._userFunctions;
	}

	get libraryCategories()
	{
		return this._library.categories;
	}
	
	getFunction(name)
	{
		// It is a base function
		if (this.baseRFunctions.hasOwnProperty(name))
		{
			// If it is final (the sequence is complete), we return a reference
			if (this.baseRFunctions[name].isFinal())
				return this.baseRFunctions[name];

			// Otherwise, we return a clone
			return this.baseRFunctions[name].clone();
		}

		// It is a function in the library
		if (this.libraryFunctions.hasOwnProperty(name))
		{
			if (!this.instances.hasOwnProperty(name))
			{
				let code = null;
				if (this.libraryFunctions[name].hasOwnProperty("code"))
				{
					code = this.libraryFunctions[name]["code"];
				}
				const sequence = this.tokenize(this.libraryFunctions[name]["tokens"]);
				this.instances[name]
					= new compositeFunctions.Library(
						this.parseSequence(sequence),
						name,
						sequence,
						this.libraryFunctions[name]["description"],
						this.libraryFunctions[name]["args"],
						code);
			}

			// If it is final (the sequence is complete), we return a reference
			if (this.instances[name].isFinal())
				return this.instances[name];

			// Otherwise, we return a clone
			return this.instances[name].clone();
		}

		// It is a function defined by the user
		if (this.userFunctions.hasOwnProperty(name))
		{
			if (!this.instances.hasOwnProperty(name))
			{
				this.instances[name]
					= new compositeFunctions.User(
						this.parseSequence(this.userFunctions[name]["tokens"]),
						name,
						this.userFunctions[name]["tokens"],
						this.userFunctions[name]["description"]);
			}

			// If it is final (the sequence is complete), we return a reference
			if (this.instances[name].isFinal())
				return this.instances[name];

			// Otherwise, we return a clone
			return this.instances[name].clone();
		}

		throw new Error("There isn't any function with that name : " + name);
	}

	/**
	 * Parse a base-8 number encoding a sequence of tokens into a syntax tree.
	 *
	 * @param code Code for the sequence of tokens.
	 * @throws SyntaxError If the encoded sequence is not valid.
	 * @return Tree of tokens.
	 */
	parseInteger(integer, base = 8)
	{
		const sequence = [];

		while (integer > 0)
		{
			sequence.push(toChar[integer % base]);
			integer = Math.floor(integer / base);
		}

		return this.parseSequence(sequence);
	}

	/**
	 * Convert a whitespace-separated string of tokens to an array
	 * of tokens.
	 *
	 * @param tokens Sequence of tokens as a whitespace-separated string.
	 * @return Array of tokens.
	 */
	tokenize(string)
	{
		return string.trim().split(/\s+/g);
	}

	/**
	 * Parse a sequence of tokens into a syntax tree.
	 *
	 * @param tokens Sequence of tokens.
	 * @throws SyntaxError If the sequence is not well-formed.
	 * @return Valid tree of tokens.
	 */
	parseSequence(sequence)
	{
		const copy = [...sequence];
		const result = this._recParseSequence(copy);

		if (copy.length > 0)
		{
			throw new SyntaxError(`Extraneous tokens at the end of the sequence \
(“${copy.join(" ")}”)`);
		}

		result.validate();
		return result;
	}

	/**
	 * @private
	 */
	_recParseSequence(sequence)
	{
		if (sequence.length === 0)
		{
			throw new SyntaxError("Empty sequences are not valid");
		}

		let node = this.getFunction(sequence.shift());

		while (node.canAddChild() && sequence.length > 0)
		{
			node.addChild(this._recParseSequence(sequence));
		}

		return node;
	}
}

export default Container;
