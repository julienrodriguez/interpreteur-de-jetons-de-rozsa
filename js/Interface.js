import * as compositeFunctions from "./compositeFunctions/index.js";
import Evaluator from "./evaluator/Evaluator.js";

/**
 * Remove all child elements (including text elements) of a DOM
 * element.
 *
 * @param elt DOM element to clear.
 */
const clearElement = elt =>
{
	let child;

	while (child = elt.lastChild)
	{
		elt.removeChild(child);
	}
};

/**
 * Helper to format numbers for display using the browser Intl library.
 *
 * @param number Number to format.
 * @return A human-readable string representing the number.
 */
const intlFormatter = new Intl.NumberFormat();
const formatNumber = number => intlFormatter.format(number);

/**
 * Timeout after which documentation is displayed for a token on hover
 * (in milliseconds).
 */
const hoverTimeout = 1000;

class Interface
{
	constructor(container)
	{
		this.container = container;
		this.evaluator = new Evaluator();

		this.currentSequence = [];
		this.currentFunction = null;

		this.loadElements();
		this.loadTokensList();
		this.loadEventListenerSpeedUpButton();
		this.loadEventListenerSequenceSaveButton();
		this.loadEventListenerSequenceClearButton();
		this.loadEventListenerEvaluationForm();
		this.loadEventListenerSaveForm();
		this.emptyBoard();
	}

	loadElements()
	{
		this.speedUpButton = document.getElementById("speedUp");
		this.documentation = document.getElementById("documentation");

		this.sequence = document.getElementById("sequence");
		this.sequenceSave = document.getElementById("sequence-save");
		this.sequenceClear = document.getElementById("sequence-clear");
		this.sequenceMessages = document.getElementById("sequence-messages");

		this.evaluation = document.getElementById("evaluation");
		this.evaluationForm = document.getElementById("evaluation-form");
		this.evaluationFields = document.getElementById("evaluation-fields");
		this.evaluationButton = document.getElementById("evaluation-button");
		this.evaluationMessages = document.getElementById("evaluation-messages");
		this.evaluationResults = document.getElementById("evaluation-result");

		this.save = document.getElementById("save");
		this.saveForm = document.getElementById("save-form");
		this.saveFunctionName = document.getElementById("save-function-name");
		this.saveFunctionDescription = document.getElementById("save-function-description")
		this.saveMessages = document.getElementById("save-messages");

		this.libraryFunctions = document.getElementById("libraryFunctions");
		this.baseFunctions = document.getElementById("baseFunctions");
		this.userFunctions = document.getElementById("userFunctions");
	}
	
	emptyTokensList()
	{
		clearElement(this.libraryFunctions);
		clearElement(this.baseFunctions);
		clearElement(this.userFunctions);
	}

	loadTokensList()
	{
		this.baseFunctions.appendChild(
			this.createTokenBlock(this.container.baseRFunctions)
		);

		for (let {name, functions} of Object.values(this.container.libraryCategories))
		{
			const title = document.createElement("h3");
			title.textContent = name;
			this.libraryFunctions.appendChild(title);

			this.libraryFunctions.appendChild(
				this.createTokenBlock(functions)
			);
		}

		this.loadUserLibrary();
	}

	loadUserLibrary()
	{
		clearElement(this.userFunctions);

		if (Object.keys(this.container.userFunctions).length > 0)
		{
			this.userFunctions.appendChild(
				this.createTokenBlock(this.container.userFunctions)
			);
		}
		else
		{
			const hint = document.createElement("span");
			hint.classList.add("hint");
			hint.textContent = "Functions that you save will appear here and persist between sessions.";
			this.userFunctions.appendChild(hint);
		}
	}

	createTokenButton(func)
	{
		const button = document.createElement("li");
		let timer = null;
		button.appendChild(func.token);

		button.addEventListener("mouseover", () =>
		{
			timer = setTimeout(
				() => this.showDocumentation(func),
				hoverTimeout
			);
		});

		button.addEventListener("mouseout", () =>
		{
			clearTimeout(timer);
		});

		return button;
	}

	createTokenBlock(namesOfFunctions)
	{
		const names = (!(namesOfFunctions instanceof Array))
			? Object.keys(namesOfFunctions)
			: namesOfFunctions;

		const block = document.createElement("ul");
		block.classList.add("token-block");

		for (let id in names)
		{
			const name = names[id];
			const func = this.container.getFunction(name);
			const button = this.createTokenButton(func);

			button.addEventListener("click", () =>
			{
				this.currentSequence.push(name);
				this.updateSequence();

				if (this.isTokensSequenceValid())
				{
					this.onSequenceValid();
				}
			});

			block.appendChild(button);
		}

		return block;
	}

	loadEventListenerSequenceSaveButton()
	{
		this.sequenceSave.addEventListener("click", e =>
		{
			e.preventDefault();
			this.toggleSavingInterface();
		});
	}

	loadEventListenerSequenceClearButton()
	{
		this.sequenceClear.addEventListener("click", e =>
		{
			e.preventDefault();
			this.emptyBoard();
		});
	}

	loadEventListenerSpeedUpButton()
	{
		this.speedUpButton.addEventListener("click", e =>
		{
			e.preventDefault();
			compositeFunctions.Library.cheat = !compositeFunctions.Library.cheat;
			this.emptyTokensList();
			this.loadTokensList();
			this.updateSequence();
		});
	}

	loadEventListenerEvaluationForm()
	{
		this.evaluationForm.addEventListener("submit", e =>
		{
			e.preventDefault();
			this.evaluate();
		});
	}

	enableEvaluationForm()
	{
		for (let input of this.evaluationForm.elements)
		{
			input.disabled = false;
		}
	}

	disableEvaluationForm()
	{
		for (let input of this.evaluationForm.elements)
		{
			input.disabled = true;
		}
	}

	loadEventListenerSaveForm()
	{
		this.saveForm.addEventListener("submit", e =>
		{
			e.preventDefault();

			try
			{
				if (this.container.addUserFunction(new compositeFunctions.User(
					this.currentFunction,
					this.saveFunctionName.value,
					this.currentSequence,
					this.saveFunctionDescription.value
				)))
				{
					clearElement(this.saveMessages.textContent);
					this.hideSavingInterface();
					this.loadUserLibrary();
				}
			}
			catch (error)
			{
				this.saveMessages.textContent = `Error: ${error.message}.`;
			}
		});
	}

	removeEvaluationFields()
	{
		clearElement(this.evaluationFields);
	}

	emptySequence()
	{
		clearElement(this.sequence);
	}

	initSequence()
	{
		const hint = document.createElement("span");
		hint.classList.add("hint");
		hint.textContent = "Click on any token of the library below to start composing a sequence.";

		clearElement(this.sequence);
		this.sequence.appendChild(hint);
		clearElement(this.sequenceMessages);
	}

	updateSequence()
	{
		if (this.currentSequence.length === 0)
		{
			this.emptyBoard();
			return;
		}

		this.emptySequence();

		for (let i = 0; i < this.currentSequence.length; ++i)
		{
			const name = this.currentSequence[i];
			const func = this.container.getFunction(name);
			const button = this.createTokenButton(func);

			button.addEventListener("click", () =>
			{
				this.removeEvaluationFields();
				this.currentSequence.splice(i,1);
				this.updateSequence();

				if (this.currentSequence.length > 0 && this.isTokensSequenceValid())
				{
					this.onSequenceValid();
				}
			});

			this.sequence.appendChild(button);
		}
	}

	isTokensSequenceValid()
	{
		try
		{
			this.currentFunction = this.container.parseSequence(this.currentSequence);
		}
		catch (error)
		{
			if (error instanceof SyntaxError)
			{
				this.onSequenceInvalid(error);
				return false;
			}

			throw error;
		}

		this.onSequenceValid();
		return true;
	}

	onSequenceValid()
	{
		clearElement(this.sequenceMessages);
		this.removeEvaluationFields();

		if (this.currentFunction.arity > 0)
		{
			const label = document.createElement("label");
			label.textContent = "Arguments: ";
			label.htmlFor = "x0";
			this.evaluationFields.appendChild(label);
		}

		for (let i = 0; i < this.currentFunction.arity; ++i)
		{
			const input = document.createElement("input");
			input.name = "x" + i;
			input.id = "x" + i;
			input.type = "number";
			input.min = 0;
			input.value = 0;
			this.evaluationFields.appendChild(input);
		}

		this.showEvaluationInterface();
		this.enableSaveButton();
	}

	onSequenceInvalid(error)
	{
		this.sequenceMessages.textContent = `Error: ${error.message}.`;
		this.hideEvaluationInterface();
		this.hideSavingInterface();
		this.disableSaveButton();
	}

	evaluate()
	{
		let params = [];
		for (let i = 0; i < this.currentFunction.arity; ++i)
		{
			let x = document.getElementById("x" + i);
			params.push(parseInt(x.value,10));
		}

		this.evaluationResults.classList.add("pending");
		clearElement(this.evaluationMessages.textContent);
		this.disableEvaluationForm();

		this.evaluator.evaluate(
			this.currentFunction,
			params
		)
			.then(({steps, value}) =>
			{
				this.evaluationResults.textContent = value;
				this.evaluationResults.classList.remove("pending");
				this.evaluationMessages.textContent = `Evaluated in ${formatNumber(steps)} steps`;
				this.enableEvaluationForm();
			});
	}

	emptyBoard()
	{
		this.currentFunction = null;
		this.currentSequence = [];

		this.initSequence();
		this.hideEvaluationInterface();
		this.hideSavingInterface();
		this.disableSaveButton();
	}

	showDocumentation(rFunction)
	{
		clearElement(this.documentation);

		const definition = document.createElement("p");
		definition.appendChild(this.container.getFunction(rFunction.name).token);

		const equals = document.createElement("span");
		equals.classList.add("def");
		equals.textContent = " \u2254  ";
		definition.appendChild(equals);

		for (let token of rFunction.getSequence())
		{
			definition.appendChild(this.container.getFunction(token).token);
		}

		this.documentation.appendChild(definition);

		const desc = document.createElement("div");

		// Emphasize variable references
		const varRegex = /`(.*?)`/g;
		const descriptionHTML = rFunction.description.replace(varRegex, "<em>$1</em>");
		desc.innerHTML = descriptionHTML;
		this.documentation.appendChild(desc);

		if (rFunction.args.length > 0)
		{
			desc.innerHTML += `<br>Argument${rFunction.args.length > 1 ? "s" : ""}:`;
			const args = document.createElement("dl");

			for (let {name, description: text} of rFunction.args)
			{
				const nameNode = document.createElement("dt");
				nameNode.textContent = name;
				args.appendChild(nameNode);

				const textNode = document.createElement("dd");
				textNode.textContent = text;
				args.appendChild(textNode);
			}

			this.documentation.appendChild(args);
		}
	}

	showSavingInterface()
	{
		this.save.classList.remove("hide");
	}

	hideSavingInterface()
	{
		this.saveFunctionName.value = "";
		this.saveFunctionDescription.value = "";
		clearElement(this.saveMessages);

		this.save.classList.add("hide");
	}

	toggleSavingInterface()
	{
		if (this.save.classList.contains("hide"))
		{
			this.showSavingInterface();
		}
		else
		{
			this.hideSavingInterface();
		}
	}

	showEvaluationInterface()
	{
		this.evaluation.classList.remove("hide");
	}

	hideEvaluationInterface()
	{
		clearElement(this.evaluationResults);
		this.evaluationResults.classList.remove("pending");
		clearElement(this.evaluationMessages);

		this.evaluation.classList.add("hide");
	}

	enableSaveButton()
	{
		this.sequenceSave.disabled = false;
	}

	disableSaveButton()
	{
		this.sequenceSave.disabled = true;
	}
}

export default Interface;
