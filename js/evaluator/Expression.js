/**
 * Expression that have a value.
 */
class Expression
{
	/**
	 * Construct a new expression.
	 *
	 * @param value Value of the expression, either an integer or a
	 * token sequence.
	 * @param [args=[]] If the expression contains a token sequence,
	 * list of arguments that are to be applied to the function
	 * defined by the sequence.
	 */
	constructor(value, args = [])
	{
		this.value = value;
		this.args = args;
	}

	/**
	 * Get whether this expression can be further reduced or not.
	 *
	 * @return {bool} True if and only if this expression can be reduced.
	 */
	get reducible()
	{
		return typeof this.value !== "number";
	}

	/**
	 * Reduce this expression to a single value.
	 *
	 * @return Number of reductions performed.
	 */
	reduce()
	{
		const stack = [this];
		let iterations = 0;

		while (this.reducible)
		{
			const elt = stack.pop();
			const deps = elt.value
				.reduceAfter(elt)
				.filter(child => child.reducible);

			if (deps.length === 0)
			{
				elt.value.reduce(elt);

				if (elt.reducible)
				{
					stack.push(elt);
				}
			}
			else
			{
				stack.push(elt);
				stack.push(...deps);
			}

			++iterations;
		}

		return iterations;
	}
}

export default Expression;
