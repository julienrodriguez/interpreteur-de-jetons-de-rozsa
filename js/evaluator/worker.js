import Container from '../Container.js';
import Expression from './Expression.js';

const container = new Container();

onmessage = evt =>
{
	const message = evt.data;
	const {id, sequence, args} = message;

	const func = container.parseSequence(sequence);
	const expression = new Expression(
		func,
		args.map(arg => new Expression(arg))
	);
	const steps = expression.reduce();

	postMessage({
		id,
		steps,
		value: expression.value
	});
};
