import Expression from './Expression.js';

class Evaluator
{
	constructor()
	{
		// Web worker thread that performs evaluations
		this.worker = new Worker("/js/worker.js", {
			type: 'module'
		});

		this.worker.onmessage = this._messageHandler.bind(this);

		// Identifier to be used for the next evaluation
		this.nextId = 0;

		// Expressions that are pending evaluation and their
		// associated promise callbacks
		this.pending = {};
	}

	/**
	 * Evaluate a function on an array of arguments.
	 *
	 * @param {Array.<number>} args Arguments to apply.
	 * @return {[number, number]} Resulting value and number of reduction steps
	 * performed to compute it.
	 */
	evaluate(func, args)
	{
		return new Promise(
			(res, rej) =>
			{
				const id = this.nextId;
				this.nextId++;
				this.pending[id] = {res, rej};

				// Ask the worker to evaluate the expression
				this.worker.postMessage({
					id,
					sequence: func.sequence,
					args,
				});
			}
		);
	}

	/**
	 * Handler of messages incoming from the worker.
	 *
	 * @param evt Event containing the received message.
	 */
	_messageHandler(evt)
	{
		const message = evt.data;
		const {id} = message;
		const callbacks = this.pending[id];

		if (callbacks === undefined)
		{
			console.warn(`Received a result for an unknown evaluation request \
(with identifier ${id}).`);
			return;
		}

		callbacks.res({
			steps: message.steps,
			value: message.value,
		});
	}
}

export default Evaluator;
